from bluetooth import *
#from bluetooth.ble import
import bluetooth.ble
from isf.core import logger
import asyncio
from bleak import discover


class BLEtool:

    def __init__(self):
        pass

    async def device_find(self):
        devices = await discover()
        for d in devices:
            print(d)

    def scanner(self):
        loop = asyncio.get_event_loop()
        loop.run_until_complete(self.device_find())
        '''
        try:
            nearby_devices = discover_devices(lookup_names=True)
            logger.info('Found devices:' + str(nearby_devices))
        except OSError:
            logger.error('Error with HCI bluetooth device')
            return []
        result = []
        for device in nearby_devices:
            result.append({'address': device[0], 'name': device[1]})
        logger.info(str(result))'''
